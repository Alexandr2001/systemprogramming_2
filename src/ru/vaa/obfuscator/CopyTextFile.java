package ru.vaa.obfuscator;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class CopyTextFile {
    private static Scanner scan = new Scanner(System.in);

    private static final String PATH_TO_THE_FILE = "Введите полный путь к файлу\nпример: C:\\Work\\file.txt\n>> ";


    public static void main(String[] args) throws InterruptedException {

        System.out.println("ПЕРВЫЙ ФАЙЛ! ");
        final String pathTheFirstFile = pathToTheFile();
        final String endPathTheFirstFile = pathToTheFile();

        System.out.println("ВТОРОЙ ФАЙЛ!");
        final String pathTheSecondFile = pathToTheFile();
        final String endPathTheSecondFile = pathToTheFile();

        final long before = System.currentTimeMillis();

        Thread oneFile = new Thread(() -> writeToNewFile(pathTheFirstFile, endPathTheFirstFile));
        Thread twoFile = new Thread(() -> writeToNewFile(pathTheSecondFile, endPathTheSecondFile));
        oneFile.start();
        twoFile.start();
        oneFile.join();
        twoFile.join();
        final long after = System.currentTimeMillis();
        System.out.printf("time delta: %d\n", (after - before));


        final long timeMillis = System.currentTimeMillis();
        oneFile.run();
        twoFile.run();
        final long currentTimeMillis = System.currentTimeMillis();
        System.out.printf("time delta: %d\n", (currentTimeMillis - timeMillis));
    }

    private static void writeToNewFile(String beginTheFile, String endPathTheFile) {
        try (FileInputStream inputStream = new FileInputStream(beginTheFile);
             FileOutputStream outputStream = new FileOutputStream(endPathTheFile)) {
            byte[] bytes = new byte[inputStream.available()];
            int length;

            while ((length = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, length);
            }

        } catch (IOException e) {
            e.getMessage();
        }
    }

    private static String pathToTheFile() {
        System.out.print(PATH_TO_THE_FILE);
        return scan.nextLine().trim();
    }
}