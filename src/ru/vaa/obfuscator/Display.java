package ru.vaa.obfuscator;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

class Display extends JFrame {

    private static final int WIDTH = 410;
    private static final int HEIGHT = 150;

    Display() {
        setTitle("Обфускатор");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(WIDTH, HEIGHT);
        setLocationRelativeTo(null);
        setResizable(false);

        Container container = getContentPane();
        container.setLayout(new FlowLayout(FlowLayout.LEFT));

        final JLabel label = new JLabel("Выбрать файл: ");
        container.add(label);

        JTextField pathToFile = new JTextField(15);
        container.add(pathToFile);

        JButton openDir = new JButton("...");
        openDir.setPreferredSize(new Dimension(20, 18));
        container.add(openDir);
        openDir.addActionListener(e -> {
            JFileChooser fileOpen = new JFileChooser();
            int ret = fileOpen.showDialog(null, "Выберите файл");
            if (ret == JFileChooser.APPROVE_OPTION) {
                File file = fileOpen.getSelectedFile();
                pathToFile.setText(file.getAbsolutePath());
            }
        });

        JButton copyFile = new JButton("Рез.копия");
        container.add(copyFile);
        copyFile.addActionListener(e -> {
            if (pathToFile.getText().equals("")) {
                JOptionPane.showMessageDialog(this, "Введите путь до файла!", "Ошибка", JOptionPane.ERROR_MESSAGE);
            } else {
                Path dir = Paths.get(pathToFile.getText());
                File newDir = new File(dir.getParent() + "\\Резервная копия");
                if (!newDir.exists()) newDir.mkdir();
                try {
                    Files.copy(dir, Paths.get(newDir.getAbsolutePath() + "\\" + dir.getFileName()));
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                JOptionPane.showMessageDialog(this, "Готово", "Уведомление", JOptionPane.WARNING_MESSAGE);
            }
        });

        JButton removeComment = new JButton("коментарии");
        container.add(removeComment);
        removeComment.addActionListener(e -> new Obfuscator(pathToFile.getText(), "comment"));

        JButton removeSpace = new JButton("пробелы");
        container.add(removeSpace);
        removeSpace.addActionListener(e -> new Obfuscator(pathToFile.getText(), "space"));

        JButton replaceNameClass = new JButton("классы");
        container.add(replaceNameClass);
        replaceNameClass.addActionListener(e -> new Obfuscator(pathToFile.getText(), "class"));

        JButton replaceNameIdentifier = new JButton("переменные");
        container.add(replaceNameIdentifier);
        replaceNameIdentifier.addActionListener(e -> new Obfuscator(pathToFile.getText(), "identifier"));
        setVisible(true);
    }
}
