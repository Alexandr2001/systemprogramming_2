package ru.vaa.obfuscator;

import java.io.*;
import java.util.stream.Collectors;

class Obfuscator {

    Obfuscator(String file, String value) {
        switch (value) {
            case "comment": {
                removeComments(file);
                break;
            }
            case "space": {

            }
        }
    }

    private void removeComments(String file) {
        String result = "";
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            result = br.lines().collect(Collectors.joining("\n"));
            result = result.replaceAll("(?sm)(^(?:\\s*)?((?:/\\*(?:\\*)?).*?(?<=\\*/))|(?://).*?(?<=$))", "");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            bw.write(result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
